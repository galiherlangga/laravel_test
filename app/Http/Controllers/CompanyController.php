<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Http\Requests\CompanyRequest;
use Exception;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_action = route('companies.store');
        $page_title = 'Create Company';
        $page_type = 'create';
        return view('companies.form', [
            'form_action'=>$form_action,
            'page_title'=>$page_title,
            'page_type'=>$page_type
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $newCompany = $request->validated();
        try {
            // save new company data
            $imageName = '';
            if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('storage'), $imageName);
                $imageName;
            }
            $company = new Company();
            $company->name = $newCompany['name'];
            $company->email = $newCompany['email'];
            $company->logo = $imageName;
            $company->website = $newCompany['website'];
            $company->save();

            if ($company) {
                error_log('success');
                return redirect()->route('companies.index')->with('success','New Company created');
            }else {
                error_log('failed');
                return redirect()->route('companies.index')->with('error','New Company failed to create');
            }
        }catch (Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return json_encode($company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $form_action = route('companies.update', $company);
        $page_title = 'Edit Company';
        $page_type = 'edit';
        return view('companies.form', [
            'company'=>$company,
            'form_action'=>$form_action,
            'page_title'=>$page_title,
            'page_type'=>$page_type
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $newCompany = $request->validated();
        $company = Company::find($id);
        try {
            // save new company data
            $imageName = $company->logo;
            if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('storage'), $imageName);
                $imageName;
            }
            $company->name = $newCompany['name'];
            $company->email = $newCompany['email'];
            $company->logo = $imageName;
            $company->website = $newCompany['website'];
            $company->save();

            if ($company) {
                error_log('success');
                return redirect()->route('companies.index')->with('success','Company has been successfully updated');
            }else {
                error_log('failed');
                return redirect()->route('companies.index')->with('error','Company failed to update');
            }
        }catch (Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company = Company::find($id);
            $company->delete();
            return redirect()->route('companies.index')->with('success', 'Company has been successfully deleted');
        } catch (Exception $e){
            return redirect()->route('companies.index')->with('error', 'Company failed to delete');
        }
    }

    /**
     * AJAX to get companies for Data Table
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCompanies(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // total number of rows per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnName = $columnName == 0 ? 'name' : $columnName;
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value


        // Total records
        $totalRecords = Company::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Company::select('count(*) as allcount')->where('name', 'like', '%' . $searchValue . '%')->count();

        $records = Company::orderBy($columnName, $columnSortOrder)
                ->where('companies.name', 'like', '%' . $searchValue . '%')
                ->orWhere('companies.email', 'like', '%' . $searchValue . '%')
                ->select('companies.*')
                ->skip($start)
                ->take($rowperpage)
                ->get();


        $data = array();

        foreach ($records as $record) {
            $data[] = array(
                'id' => $record->id,
                'name' => $record->name,
                'email' => $record->email,
                'logo' => "<img src='storage/$record->logo' loading='lazy' class='img-thumbnail'>",
                'website' => "<a href='$record->website' target='_blank'>$record->website</a>",
                'action'=>"<a href='/companies/$record->id/edit' class='btn btn-primary mt-1'>Edit</a> <button data-target='/companies/$record->id' data-name='$record->name' class='btn btn-danger delete mt-1'>Delete</button>"
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            'aaData' => $data,
        );
        echo json_encode($response);
    }
}
