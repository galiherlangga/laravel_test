<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Mail\NewEmployee;
use App\Models\Company;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        $form_action = route('employees.store');
        $page_title = 'Create Employee';
        $page_type = 'create';
        return view('employees.form', [
            'form_action'=>$form_action,
            'page_title'=>$page_title,
            'page_type'=>$page_type,
            'companies'=>$companies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $newEmployee = $request->validated();
        // dd($newEmployee);
        // try{
            $employee = new Employee();
            $employee->first_name = $newEmployee['first_name'];
            $employee->last_name = $newEmployee['last_name'];
            $employee->company_id = $newEmployee['company'];
            $employee->email = $newEmployee['email'];
            $employee->phone = $newEmployee['phone'];
            $employee->save();
            error_log('saved');
            error_log($employee->company->email);
            Mail::to($employee->company->email)->send(new NewEmployee());
            error_log('send');

            if ($employee){
                error_log('success');
                return redirect()->route('employees.index')->with('success','New Employee created');
            }else {
                error_log('failed');
                return redirect()->route('employees.index')->with('error','New Employee failed to create');
            }
        // }catch (Exception $e){
        //     return redirect()->back()->with('error', $e);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $companies = Company::all();
        $form_action = route('employees.update', $employee);
        $page_title = 'Edit Employee';
        $page_type = 'edit';
        return view('employees.form', [
            'form_action'=>$form_action,
            'page_title'=>$page_title,
            'page_type'=>$page_type,
            'companies'=>$companies,
            'employee'=>$employee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $newEmployee = $request->validated();
        // dd($newEmployee);
        try{
            $employee = Employee::find($id);
            $employee->first_name = $newEmployee['first_name'];
            $employee->last_name = $newEmployee['last_name'];
            $employee->company_id = $newEmployee['company'];
            $employee->email = $newEmployee['email'];
            $employee->phone = $newEmployee['phone'];
            $employee->save();

            if ($employee){
                error_log('success');
                return redirect()->route('employees.index')->with('success','Employee has been successfully updated');
            }else {
                error_log('failed');
                return redirect()->route('employees.index')->with('error','Employee failed to update');
            }
        }catch (Exception $e){
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = Employee::find($id);
            $employee->delete();
            return redirect()->route('employees.index')->with('success', 'Employee has been successfully deleted');
        } catch (Exception $e){
            return redirect()->route('employees.index')->with('error', 'Employee failed to delete');
        }
    }


    /**
     * AJAX to get companies for Data Table
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEmployees(Request $request)
    {
        // return 'test';
        // dd('test');
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // total number of rows per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnName = $columnName == 0 ? 'first_name' : $columnName;
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value


        // Total records
        $totalRecords = Employee::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Employee::select('count(*) as allcount')->where('first_name', 'like', '%' . $searchValue . '%')->count();

        $records = Employee::orderBy($columnName, $columnSortOrder)
                ->where('employees.first_name', 'like', '%' . $searchValue . '%')
                ->orWhere('employees.last_name', 'like', '%' . $searchValue . '%')
                ->orWhere('employees.email', 'like', '%' . $searchValue . '%')
                ->orWhere('employees.phone', 'like', '%' . $searchValue . '%')
                ->select('employees.*')
                ->skip($start)
                ->take($rowperpage)
                ->get();

        $data = array();

        foreach ($records as $record) {
            $data[] = array(
                'id' => $record->id,
                'fullname'=> $record->first_name.' '.$record->last_name,
                'company'=> "<button class='btn btn-primary view' data-target='/companies/$record->company_id'>".$record->company->name."</button>",
                'email'=>$record->email,
                'phone'=>$record->phone,
                'action'=>"<a href='/employees/$record->id/edit' class='btn btn-primary mt-1'>Edit</a> <button data-target='/employees/$record->id' data-name='$record->name' class='btn btn-danger delete mt-1'>Delete</button>"
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            'aaData' => $data,
        );
        echo json_encode($response);
        // return $response;
    }
}
