@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$page_title}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">{{$page_title}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$page_title}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <form action="{{$form_action}}" method='POST' enctype="multipart/form-data">
                    @if ($page_type=='edit')
                    @method('PUT')
                    @endif
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name='first_name' class='form-control @error('first_name')is-invalid @enderror'
                                    value="{{$employee->first_name??old('first_name')}}" placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" name='last_name' class='form-control @error('last_name')is-invalid @enderror'
                                    value="{{$employee->last_name??old('last_name')}}" placeholder="Last Name" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="company">Company</label>
                                <select name="company" id="company" class="form-control @error('company')is-invalid @enderror" required>
                                    <option value="">- Select Company -</option>
                                    @foreach ($companies as $company)
                                    <option value="{{$company->id}}" {{ isset($employee->company_id)?($employee->company_id==$company_id?'selected':''):(old('company')==$company->id?'selected':'') }}>{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name='email' class='form-control @error('email')is-invalid @enderror'
                                    value="{{$employee->email??old('email')}}" placeholder="e.g. test@laravel.com">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" name='phone' class='form-control @error('phone')is-invalid @enderror'
                                    value="{{$employee->phone??old('phone')}}" placeholder="Phone">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group float-right mt-3">
                                <a href="{{ route('employees.index') }}" class="btn btn-danger">Cancel</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
@endsection