@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Employees</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Employees</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Employees</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="row">
                    <div class="col-3 mb-3">
                        <a href="{{ route('employees.create') }}" class="btn btn-primary"><i
                                class="fa fa-plus"></i>&nbsp;Create&nbsp;Employees</a>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Full Name</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <form action="" id='form-delete' method="POST">
                            @method('DELETE')
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id='delete-text'></p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id='delete-button'>Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id='modal-view-title'></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <img src="" alt="company logo" id='company-logo' class="img-thumbnail mx-auto">
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <p><b>Name</b></p>
                    </div>
                    <div class="col-8">
                        <p id='company-name'></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <p><b>Email</b></p>
                    </div>
                    <div class="col-8">
                        <p id='company-email'></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <p><b>Website</b></p>
                    </div>
                
                    <div class="col-8">
                        <p id='company-website'></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id='delete-button'>Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('script')
<script>
    $(function(){
        $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            autoWidth: false,
            ajax: "{{ route('employees.getEmployees') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'fullname', name: 'fullname'},
                {data: 'company', name: 'company'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        })

        $('body').on('click','.delete', function(){
            let target = $(this).data('target')
            let name = $(this).data('name')
            console.log(target)
            $('#delete-text').text('Are you sure you want to delete the '+name+'?')
            $('#modal-delete').modal('show')
            $('#form-delete').attr('action',target)
        })

        $('body').on('click', '.view', function() {
            let target = $(this).data('target')
            $.ajax({
                url: target,
                method: 'GET',
                success:function(data){
                    data = JSON.parse(data)
                    console.log(data)
                    $('#company-logo').attr('src', 'storage/'+data.logo)
                    $('#company-name').text(data.name)
                    $('#company-email').text(data.email)
                    $('#company-website').text(data.website)
                    $('#modal-view-title').text(data.name+' Company')
                }
            })
            $('#modal-view').modal('show')
        })

        $('#delete-button').click(function(){
            $('#form-delete').submit()
        })
    })
</script>
@endsection