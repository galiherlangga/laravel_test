<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
                <i class="nav-icon fas fa-columns"></i>
                <p>Home</p>
            </a>
        </li>
        @if (auth()->user()->role=='admin')
        <li class="nav-item">
            <a href="{{ route('companies.index') }}" class="nav-link">
                <i class="nav-icon fas fa-building"></i>
                <p>Companies</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('employees.index') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>Employees</p>
            </a>
        </li>
        @endif
        <li class="nav-item">
            <form action="{{ route('logout') }}" method="POST" id='logout-form'>@csrf
                <a href='javascript:void(0)' class="nav-link" id='logout-btn'>
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    <p>Logout</p>
                </a>
            </form>
        </li>
    </ul>
</nav>