@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$page_title}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">{{$page_title}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$page_title}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <form action="{{$form_action}}" method='POST' enctype="multipart/form-data">
                    @if ($page_type=='edit')
                    @method('PUT')
                    @endif
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Company Name</label>
                                <input type="text" name='name' class='form-control @error(' name')is-invalid @enderror'
                                    value="{{$company->name??old('company')}}" placeholder="Company Name" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Company Email</label>
                                <input type="email" name='email' class='form-control'
                                    value="{{$company->email??old('email')}}" placeholder="e.g. test@laravel.com">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="logo">Company Logo</label>
                                <input type="file" name="logo" id="logo" class="form-control" accept="image/*">
                                <div class="row" style="margin-top:5px">
                                    <div class="col-md-3">
                                        <img src="{{ isset($company->logo)?asset('storage/'.$company->logo):asset('img/no-image.jpg') }}"
                                            id='display-image' alt="no-image" srcset="" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="website">Company Website</label>
                                <input type="text" name='website' class='form-control'
                                    value="{{$company->website??old('website')}}"
                                    placeholder="e.g. http://laraveltest.com">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group float-right mt-3">
                                <a href="{{ route('companies.index') }}" class="btn btn-danger">Cancel</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
@endsection

@section('script')
<script>
    $("body").on("change", "#logo", function () {
        var defaultsrc = '/img/no-image.jpg';
        var input = this;
        var url = $(this).val();
        console.log(url);
        var ext = url.substring(url.lastIndexOf(".") + 1).toLowerCase();
        if (
            input.files &&
            input.files[0] &&
            (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")
        ) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#display-image").attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else {
            $("#display-image").attr("src", defaultsrc);
        }
    });
</script>
@endsection