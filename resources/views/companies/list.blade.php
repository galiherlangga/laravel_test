@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Companies</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Companies</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Companies</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="row">
                    <div class="col-3 mb-3">
                        <a href="{{ route('companies.create') }}" class="btn btn-primary"><i
                                class="fa fa-plus"></i>&nbsp;Create&nbspCompanies</a>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>Website</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <form action="" id='form-delete' method="POST">
                            @method('DELETE')
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Company</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id='delete-text'></p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id='delete-button'>Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('script')
<script>
    $(function(){

        $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            autoWidth: false,
            ajax: "{{ route('companies.getCompanies') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'logo', name: 'logo'},
                {data: 'website', name: 'website'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        })

        $('body').on('click','.delete', function(){
            let target = $(this).data('target')
            let name = $(this).data('name')
            console.log(target)
            $('#delete-text').text('Are you sure you want to delete the '+name+' company?')
            $('#modal-delete').modal('show')
            $('#form-delete').attr('action',target)
        })

        $('#delete-button').click(function(){
            $('#form-delete').submit()
        })

    })
</script>
@endsection