<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name'=>'GRTech',
            'email'=>'info@grtech.com.my',
            'logo'=>'logo.png',
            'website'=>'https://grtech.com'
        ]);
    }
}
