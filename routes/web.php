<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::group(['middleware'=>['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::middleware(['login.role:admin'])->group(function(){
        // Companies route
        Route::resource('/companies', CompanyController::class);
        Route::get('/comapnies/getCompanies', [CompanyController::class, 'getCompanies'])->name('companies.getCompanies');

        // Employees route
        Route::get('/employees/getEmployees', [EmployeeController::class, 'getEmployees'])->name('employees.getEmployees');
        Route::resource('/employees', EmployeeController::class);
    });
    
});
